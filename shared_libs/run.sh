# Creating shared library and using them

rm -rf lib.o lib.so
# Method 1
gcc -c  -fpic foo.c 
gcc -shared -o libfoo.so foo.o 
gcc -L./ -o test main.c -lfoo 
ldd ./test
LD_LIBRARY_PATH=./ ./test

rm -rf lib.o lib.so
echo
# Method 2
gcc -c  -fpic foo.c 
gcc -shared -o libfoo.so foo.o 
gcc -L./ -o test main.c -lfoo -Wl,-v,-rpath,./ 
ldd ./test
./test

rm -rf lib.o lib.so
echo
# Wrong Method
gcc -shared  -c  -fpic  foo.c  -o libfoo.so 
gcc -L./ -o test main.c -lfoo 
ldd ./test
./test
